# windows-debug-user

Creates a windows user based on the parameters:
* SRC_USER
* SRC_PW

Thesse parameters can either be set as fixed or overwritten in an application or as interactive parameters.

When left empty the plugins won't work.

This user will be added to the "administrators" and "Remote Desktop Users" group.

**This plugin is for debugging purposes, it is less secure than the normal TOTP based login**