
$LOGFILE = "c:\logs\windows-debug-user.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {

  Write-Log "Startwindows-debug-user"
 
  try {
    $user = [System.Environment]::GetEnvironmentVariable('SRC_USER')
    $pass = [System.Environment]::GetEnvironmentVariable('SRC_PW') | ConvertTo-SecureString -AsPlainText -Force
    
    if ($user -and $pass) {
      New-LocalUser -Name $user -Password $pass

      Add-LocalGroupMember -Group "Administrators" -Member $user
      Add-LocalGroupMember -Group "Remote Desktop Users" -Member $user
    } else { 
      Write-Log "SRC_USER or SRC_PW is empty"
    }
  }
  catch {
      Write-Log "$_"
      Throw $_
  }
  
  Write-Log "End windows-debug-user"
 
}

Main    